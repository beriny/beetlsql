package org.beetl.sql.saga.ms.server.util;

/**
 * 业务自身是否操作成功
 */
public enum BusinessStatus {
	Success,
	Error
}
